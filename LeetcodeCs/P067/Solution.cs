﻿using System;

namespace LeetcodeCs.P067
{
    public class Solution
    {
        public string AddBinary(string a, string b)
        {
            var numDigits = Math.Max(a.Length, b.Length);
            var builder = new char[numDigits + 1];

            var i = a.Length;
            var j = b.Length;
            var carry = 0;

            for (var k = numDigits; k >= 0; k--)
            {
                var digit = carry;
                if (i > 0) digit += a[--i] - '0';
                if (j > 0) digit += b[--j] - '0';

                if (digit >= 2)
                {
                    carry = 1;
                    digit -= 2;
                }
                else
                {
                    carry = 0;
                }

                builder[k] = (char)(digit + '0');
            }

            var result = builder[0] == '0'
                ? new string(builder, 1, numDigits)
                : new string(builder);

            return result;
        }
    }
}
