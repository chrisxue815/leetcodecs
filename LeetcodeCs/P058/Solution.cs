﻿namespace LeetcodeCs.P058
{
    public class Solution
    {
        public int LengthOfLastWord(string s)
        {
            var reinit = false;
            var count = 0;

            foreach (var ch in s)
            {
                if (ch == ' ')
                {
                    reinit = true;
                }
                else if (reinit)
                {
                    count = 1;
                    reinit = false;
                }
                else
                {
                    count++;
                }
            }

            return count;
        }
    }
}
