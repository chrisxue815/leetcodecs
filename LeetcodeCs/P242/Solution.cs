﻿namespace LeetcodeCs.P242
{
    public class Solution
    {
        public bool IsAnagram(string s, string t)
        {
            if (s.Length != t.Length) return false;

            const int numChars = 26;
            var charCount1 = new int[numChars];
            var charCount2 = new int[numChars];

            foreach (var ch in s)
            {
                charCount1[ch - 'a']++;
            }

            foreach (var ch in t)
            {
                charCount2[ch - 'a']++;
            }

            for (var i = 0; i < numChars; i++)
            {
                if (charCount1[i] != charCount2[i]) return false;
            }

            return true;
        }
    }
}
