﻿using LeetcodeCs.Utils;
using System.Collections.Generic;

namespace LeetcodeCs.P101
{
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     public int val;
     *     public TreeNode left;
     *     public TreeNode right;
     *     public TreeNode(int x) { val = x; }
     * }
     */

    public class Solution
    {
        public bool IsSymmetric(TreeNode root)
        {
            if (root == null) return true;

            var queue = new Queue<TreeNode>();

            queue.Enqueue(root.left);
            queue.Enqueue(root.right);

            while (queue.Count > 0)
            {
                var left = queue.Dequeue();
                var right = queue.Dequeue();

                if (left == null)
                {
                    if (right == null)
                    {
                        continue;
                    }
                }
                else if (right != null && left.val == right.val)
                {
                    queue.Enqueue(left.left);
                    queue.Enqueue(right.right);
                    queue.Enqueue(left.right);
                    queue.Enqueue(right.left);
                    continue;
                }

                return false;
            }

            return true;
        }
    }
}
