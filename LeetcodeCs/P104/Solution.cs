﻿using LeetcodeCs.Utils;
using System;

namespace LeetcodeCs.P104
{
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     public int val;
     *     public TreeNode left;
     *     public TreeNode right;
     *     public TreeNode(int x) { val = x; }
     * }
     */

    public class Solution
    {
        public int MaxDepth(TreeNode root)
        {
            if (root == null) return 0;
            var leftMax = MaxDepth(root.left);
            var rightMax = MaxDepth(root.right);
            return Math.Max(leftMax, rightMax) + 1;
        }
    }
}
