﻿namespace LeetcodeCs.P036
{
    public class Solution
    {
        public bool IsValidSudoku(char[,] board)
        {
            var width = board.GetLength(1);
            var height = board.GetLength(0);
            var row = new bool[width];
            var column = new bool[height, width];
            var box = new bool[width / 3, 9];

            for (var i = 0; i < 3; i++)
            {
                for (var j = 0; j < 3; j++)
                {
                    for (var k = 0; k < 9; k++)
                    {
                        var ch = board[i * 3 + j, k];
                        if (ch == '.') continue;

                        var num = ch - '0' - 1;

                        if (row[num]) return false;
                        row[num] = true;

                        if (column[k, num]) return false;
                        column[k, num] = true;

                        if (box[k / 3, num]) return false;
                        box[k / 3, num] = true;
                    }
                    row = new bool[9];
                }
                box = new bool[3, 9];
            }

            return true;
        }
    }
}
