﻿using LeetcodeCs.Utils;

namespace LeetcodeCs.P083
{
    /**
     * Definition for singly-linked list.
     * public class ListNode {
     *     public int val;
     *     public ListNode next;
     *     public ListNode(int x) { val = x; }
     * }
     */

    public class Solution
    {
        public ListNode DeleteDuplicates(ListNode head)
        {
            if (head == null) return null;

            var cur = head;
            var next = cur.next;

            while (next != null)
            {
                if (next.val != cur.val)
                {
                    cur.next = next;
                    cur = next;
                    next = next.next;
                }
                else
                {
                    next = next.next;
                }
            }

            cur.next = next;

            return head;
        }
    }
}
