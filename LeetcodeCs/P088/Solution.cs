﻿using System;

namespace LeetcodeCs.P088
{
    public class Solution
    {
        public void Merge(int[] nums1, int m, int[] nums2, int n)
        {
            var a = nums1.Length - 1;
            for (var b = m - 1; b >= 0; b--)
            {
                nums1[a] = nums1[b];
                a--;
            }

            var i = nums1.Length - m;
            var j = 0;
            var writeIndex = 0;

            while (i < nums1.Length && j < n)
            {
                if (nums1[i] < nums2[j])
                {
                    var k = i + 1;
                    for (; k < nums1.Length; k++)
                    {
                        if (nums1[k] > nums2[j]) break;
                    }
                    var len = k - i;
                    Array.Copy(nums1, i, nums1, writeIndex, len);
                    i += len;
                    writeIndex += len;
                }
                else
                {
                    var k = j + 1;
                    for (; k < n; k++)
                    {
                        if (nums2[k] > nums1[i]) break;
                    }
                    var len = k - j;
                    Array.Copy(nums2, j, nums1, writeIndex, len);
                    j += len;
                    writeIndex += len;
                }
            }

            if (i < nums1.Length)
            {
                Array.Copy(nums1, i, nums1, writeIndex, nums1.Length - i);
            }
            else if (j < n)
            {
                Array.Copy(nums2, j, nums1, writeIndex, n - j);
            }
        }
    }
}
