﻿namespace LeetcodeCs.P028
{
    public class Solution
    {
        public int StrStr(string haystack, string needle)
        {
            if (needle.Length == 0) return 0;
            if (haystack.Length == 0) return -1;

            var next = CreateNext(needle);
            var i = 0;
            var j = 0;

            while (i < haystack.Length)
            {
                if (haystack[i] == needle[j])
                {
                    if (j == needle.Length - 1)
                    {
                        return i - j;
                    }
                    i++;
                    j++;
                }
                else
                {
                    j = next[j];
                    if (j == -1)
                    {
                        i++;
                        j = 0;
                    }
                }
            }

            return -1;
        }

        private static int[] CreateNext(string needle)
        {
            var next = new int[needle.Length];

            next[0] = -1;
            var k = -1;
            var j = 0;

            while (j < needle.Length - 1)
            {
                if (k == -1 || needle[j] == needle[k])
                {
                    j++;
                    k++;
                    next[j] = k;
                }
                else
                {
                    k = next[k];
                }
            }

            return next;
        }
    }
}
