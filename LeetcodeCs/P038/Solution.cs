﻿using System.Text;

namespace LeetcodeCs.P038
{
    public class Solution
    {
        public string CountAndSay(int n)
        {
            if (n == 0) return "";
            var seq = new StringBuilder("1");

            while (--n > 0)
            {
                var builder = new StringBuilder((int)(seq.Length * 1.5f));
                var count = 1;
                var prev = seq[0];
                for (var i = 1; i < seq.Length; i++)
                {
                    if (seq[i] == prev)
                    {
                        count++;
                    }
                    else
                    {
                        builder.Append(count);
                        builder.Append(prev);
                        count = 1;
                        prev = seq[i];
                    }
                }
                builder.Append(count);
                builder.Append(prev);
                seq = builder;
            }

            return seq.ToString();
        }
    }
}
