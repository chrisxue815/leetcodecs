﻿namespace LeetcodeCs.P066
{
    public class Solution
    {
        public int[] PlusOne(int[] digits)
        {
            if (digits.Length == 0) return digits;

            var carry = true;

            for (var i = digits.Length - 1; i >= 0; i--)
            {
                if (carry)
                {
                    digits[i]++;
                    carry = false;
                }
                else
                {
                    break;
                }

                if (digits[i] == 10)
                {
                    digits[i] = 0;
                    carry = true;
                }
            }

            if (carry)
            {
                var newDigits = new int[digits.Length + 1];
                newDigits[0] = 1;
                digits.CopyTo(newDigits, 1);
                digits = newDigits;
            }

            return digits;
        }
    }
}
