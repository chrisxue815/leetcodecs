﻿using System.Collections.Generic;

namespace LeetcodeCs.P070
{
    public class Solution
    {
        private readonly List<int> fibonacci = new List<int> { 0, 1, 2 };

        public int ClimbStairs(int n)
        {
            return Fibonacci(n);
        }

        private int Fibonacci(int n)
        {
            if (n < fibonacci.Count)
            {
                return fibonacci[n];
            }

            var fn = Fibonacci(n - 2) + Fibonacci(n - 1);
            fibonacci.Add(fn);
            return fn;
        }
    }
}
