﻿using LeetcodeCs.Utils;
using System.Collections.Generic;

namespace LeetcodeCs.P102
{
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     public int val;
     *     public TreeNode left;
     *     public TreeNode right;
     *     public TreeNode(int x) { val = x; }
     * }
     */

    public class Solution
    {
        public IList<IList<int>> LevelOrder(TreeNode root)
        {
            var traversal = new List<IList<int>>();

            if (root == null) return traversal;

            var level = new List<int>();

            var curQueue = new Queue<TreeNode>();
            var nextQueue = new Queue<TreeNode>();

            curQueue.Enqueue(root);

            while (curQueue.Count > 0)
            {
                var node = curQueue.Dequeue();

                level.Add(node.val);

                if (node.left != null) nextQueue.Enqueue(node.left);
                if (node.right != null) nextQueue.Enqueue(node.right);

                if (curQueue.Count == 0)
                {
                    traversal.Add(level);
                    level = new List<int>();

                    curQueue = nextQueue;
                    nextQueue = new Queue<TreeNode>();
                }
            }

            return traversal;
        }
    }
}
