﻿namespace LeetcodeCs.P026
{
    public class Solution
    {
        public int RemoveDuplicates(int[] nums)
        {
            if (nums.Length == 0) return 0;
            if (nums.Length == 1) return 1;

            var i = 0;
            for (var j = 1; j < nums.Length; j++)
            {
                if (nums[j] != nums[i])
                {
                    i++;
                    if (j != i)
                    {
                        nums[i] = nums[j];
                    }
                }
            }

            return i + 1;
        }
    }
}
