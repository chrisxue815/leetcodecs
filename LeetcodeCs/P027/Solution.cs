﻿namespace LeetcodeCs.P027
{
    public class Solution
    {
        public int RemoveElement(int[] nums, int val)
        {
            if (nums.Length == 0) return 0;

            var i = 0;
            for (var j = 0; j < nums.Length; j++)
            {
                if (nums[j] != val)
                {
                    if (i != j)
                    {
                        nums[i] = nums[j];
                    }
                    i++;
                }
            }
            return i;
        }
    }
}
