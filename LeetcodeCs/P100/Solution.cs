﻿using LeetcodeCs.Utils;

namespace LeetcodeCs.P100
{
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     public int val;
     *     public TreeNode left;
     *     public TreeNode right;
     *     public TreeNode(int x) { val = x; }
     * }
     */

    public class Solution
    {
        public bool IsSameTree(TreeNode p, TreeNode q)
        {
            if (p == q) return true;
            if (p == null || q == null || p.val != q.val) return false;

            if (!IsSameTree(p.left, q.left)) return false;
            if (!IsSameTree(p.right, q.right)) return false;

            return true;
        }
    }
}
