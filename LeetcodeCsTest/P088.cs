﻿using LeetcodeCs.P088;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P088
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            var a = new[] { 1, 2, 5, 6 };
            var b = new[] { 3, 4, 7, 8 };
            var c = new int[a.Length + b.Length];
            a.CopyTo(c, 0);
            solution.Merge(c, a.Length, b, b.Length);
            Assert.IsTrue(c.SequenceEqual(new[] { 1, 2, 3, 4, 5, 6, 7, 8 }));
        }
    }
}
