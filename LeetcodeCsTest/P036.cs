﻿using LeetcodeCs.P036;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P036
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            TestResult(false, new[]
            {
                "....5..1.",
                ".4.3.....",
                ".....3..1",
                "8......2.",
                "..2.7....",
                ".15......",
                ".....2...",
                ".2.9.....",
                "..4......",
            });

            TestResult(false, new[]
            {
                "..4...63.",
                ".........",
                "5......9.",
                "...56....",
                "4.3.....1",
                "...7.....",
                "...5.....",
                ".........",
                ".........",
            });
        }

        private void TestResult(bool result, string[] boardStr)
        {
            var width = boardStr[0].Length;
            var height = boardStr.Length;
            var board = new char[height, width];

            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    board[i, j] = boardStr[i][j];
                }
            }

            Assert.AreEqual(result, solution.IsValidSudoku(board));
        }
    }
}
