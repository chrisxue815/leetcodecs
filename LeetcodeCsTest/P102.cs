﻿using LeetcodeCs.P102;
using LeetcodeCs.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P102
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            var root = new TreeNode(3);
            root.left = new TreeNode(9);
            root.right = new TreeNode(20);
            root.right.left = new TreeNode(15);
            root.right.right = new TreeNode(7);

            var levelOrder = solution.LevelOrder(root);
            var result = new[]
            {
                new[] {3},
                new[] {9, 20},
                new[] {15, 7},
            };

            for (var i = 0; i < levelOrder.Count; i++)
            {
                Assert.IsTrue(levelOrder[i].SequenceEqual(result[i]));
            }
        }
    }
}
