﻿using LeetcodeCs.P067;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P067
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            Assert.AreEqual("0", solution.AddBinary("0", "0"));
            Assert.AreEqual("1", solution.AddBinary("1", "0"));
            Assert.AreEqual("1", solution.AddBinary("0", "1"));
            Assert.AreEqual("10", solution.AddBinary("1", "1"));
            Assert.AreEqual("11", solution.AddBinary("10", "1"));
            Assert.AreEqual("11", solution.AddBinary("1", "10"));
            Assert.AreEqual("100", solution.AddBinary("10", "10"));
            Assert.AreEqual("101", solution.AddBinary("10", "11"));
            Assert.AreEqual("110", solution.AddBinary("11", "11"));
        }
    }
}
