﻿using LeetcodeCs.P101;
using LeetcodeCs.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P101
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            var root = new TreeNode(1);
            root.left = new TreeNode(2);
            root.right = new TreeNode(2);
            root.left.left = new TreeNode(3);
            root.left.right = new TreeNode(4);
            root.right.left = new TreeNode(4);
            root.right.right = new TreeNode(3);

            Assert.IsTrue(solution.IsSymmetric(root));

            root = new TreeNode(1);
            root.left = new TreeNode(2);
            root.right = new TreeNode(2);
            root.left.right = new TreeNode(3);
            root.right.right = new TreeNode(3);

            Assert.IsFalse(solution.IsSymmetric(root));
        }
    }
}
