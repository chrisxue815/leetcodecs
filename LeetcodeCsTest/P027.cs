﻿using LeetcodeCs.P027;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P027
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            TestResult(new int[0], 1, new int[0]);
            TestResult(new[] { 1 }, 0, new[] { 1 });
            TestResult(new[] { 1 }, 1, new int[0]);
            TestResult(new[] { 1, 1, 2, 3 }, 1, new[] { 2, 3 });
            TestResult(new[] { 1, 2, 2, 3 }, 2, new[] { 1, 3 });
            TestResult(new[] { 1, 2, 3, 3 }, 3, new[] { 1, 2 });
            TestResult(new[] { 1, 2, 3 }, 4, new[] { 1, 2, 3 });
            TestResult(new[] { 4, 5 }, 4, new[] { 5 });
        }

        private void TestResult(int[] arr, int val, int[] result)
        {
            Assert.AreEqual(result.Length, solution.RemoveElement(arr, val));

            if (arr.Length < result.Length)
            {
                Assert.Fail();
            }

            for (var i = 0; i < result.Length; i++)
            {
                if (arr[i] != result[i])
                {
                    Assert.Fail();
                }
            }
        }
    }
}
