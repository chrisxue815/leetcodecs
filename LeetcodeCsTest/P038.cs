﻿using LeetcodeCs.P038;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P038
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            Assert.AreEqual("1", solution.CountAndSay(1));
            Assert.AreEqual("11", solution.CountAndSay(2));
            Assert.AreEqual("21", solution.CountAndSay(3));
            Assert.AreEqual("1211", solution.CountAndSay(4));
            Assert.AreEqual("111221", solution.CountAndSay(5));
            Assert.AreEqual("312211", solution.CountAndSay(6));
            Assert.AreEqual("13112221", solution.CountAndSay(7));
            Assert.AreEqual("1113213211", solution.CountAndSay(8));
            Assert.AreEqual("31131211131221", solution.CountAndSay(9));
            Assert.AreEqual("13211311123113112211", solution.CountAndSay(10));
        }
    }
}
