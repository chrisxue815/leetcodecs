﻿using LeetcodeCs.P028;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P028
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();
            Assert.AreEqual(0, solution.StrStr("ABCD", "ABC"));
            Assert.AreEqual(1, solution.StrStr("ABCD", "BCD"));
            Assert.AreEqual(1, solution.StrStr("ABCD", "BC"));
            Assert.AreEqual(-1, solution.StrStr("ABCD", "BCE"));
            Assert.AreEqual(-1, solution.StrStr("BBAA", "AABB"));
            Assert.AreEqual(0, solution.StrStr("", ""));
            Assert.AreEqual(-1, solution.StrStr("", "ABC"));
            Assert.AreEqual(0, solution.StrStr("ABC", ""));
        }
    }
}
