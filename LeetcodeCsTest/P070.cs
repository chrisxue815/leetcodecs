﻿using LeetcodeCs.P070;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P070
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            Assert.AreEqual(1, solution.ClimbStairs(1));
            Assert.AreEqual(2, solution.ClimbStairs(2));
            Assert.AreEqual(3, solution.ClimbStairs(3));
            Assert.AreEqual(5, solution.ClimbStairs(4));
            Assert.AreEqual(8, solution.ClimbStairs(5));
            Assert.AreEqual(13, solution.ClimbStairs(6));
            Assert.AreEqual(21, solution.ClimbStairs(7));
        }
    }
}
