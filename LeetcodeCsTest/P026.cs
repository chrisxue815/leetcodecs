﻿using LeetcodeCs.P026;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P026
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            TestResult(new int[0], new int[0]);
            TestResult(new[] { 1 }, new[] { 1 });
            TestResult(new[] { 1, 1, 2, 3 }, new[] { 1, 2, 3 });
            TestResult(new[] { 1, 2, 2, 3 }, new[] { 1, 2, 3 });
            TestResult(new[] { 1, 2, 3, 3 }, new[] { 1, 2, 3 });
            TestResult(new[] { 1, 2, 3 }, new[] { 1, 2, 3 });
        }

        private void TestResult(int[] arr, int[] result)
        {
            Assert.AreEqual(result.Length, solution.RemoveDuplicates(arr));

            if (arr.Length < result.Length)
            {
                Assert.Fail();
            }

            for (var i = 0; i < result.Length; i++)
            {
                if (arr[i] != result[i])
                {
                    Assert.Fail();
                }
            }
        }
    }
}
