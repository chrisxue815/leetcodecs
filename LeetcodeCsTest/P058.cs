﻿using LeetcodeCs.P058;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetcodeCsTest
{
    [TestClass]
    public class P058
    {
        private Solution solution;

        [TestMethod]
        public void Test()
        {
            solution = new Solution();

            Assert.AreEqual(5, solution.LengthOfLastWord("Hello World"));
            Assert.AreEqual(0, solution.LengthOfLastWord(""));
            Assert.AreEqual(2, solution.LengthOfLastWord("AB "));
            Assert.AreEqual(3, solution.LengthOfLastWord("ABC"));
            Assert.AreEqual(4, solution.LengthOfLastWord(" ABCD"));
            Assert.AreEqual(2, solution.LengthOfLastWord("AB  "));
        }
    }
}
